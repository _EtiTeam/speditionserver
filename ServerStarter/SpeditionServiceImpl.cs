﻿using System;
using SpeditionService;
using NServiceBus;
using System.ServiceModel;

namespace SpeditionServerStarter
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    class SpeditionServiceImpl : 
        SpeditionService.SpeditionService
    {
        private IEndpointInstance endpoint;

        public override IEndpointInstance NServiceBusEndpoint { get; set; }

        public SpeditionServiceImpl(IEndpointInstance endpoint)
        {
            this.endpoint = endpoint;
        }
    }
}
