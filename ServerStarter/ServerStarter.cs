﻿using NServiceBus;
using ServiceBusDLL;
using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace SpeditionServerStarter
{
    public class ServerStarter
    {
        static void Main(string[] args)
        {
            string serverName = Properties.Settings.Default.serviceName;
            Console.Write("#########");

            var speditionService = new SpeditionServiceImpl(null);
            // main service
            var serviceHost = new ServiceHost(speditionService, new Uri($"http://localhost/{serverName}"));
            Console.Write("####");
            var endpoint = serviceHost.AddServiceEndpoint(typeof(SpeditionService.ISpeditionService), new WebHttpBinding(), "");
            Console.Write("####");
            endpoint.Behaviors.Add(new WebHttpBehavior());
            Console.Write("####");
            endpoint = serviceHost.AddServiceEndpoint(typeof(SpeditionService.IRestSpeditionService), new WebHttpBinding(), "rest");
            Console.Write("#####");
            endpoint.Behaviors.Add(new WebHttpBehavior());
            Console.Write("#########");

            // NSeviceBus
            var endpointConfiguration = ServiceBusConfigurator
                .getConfiguration(ServiceBusConstants.SPEDITION_SERVICE_NAME);

            // UserService
            var serviceHostUserService = new ServiceHost(typeof(SpeditionService.UserService), new Uri[] { new Uri($"http://localhost/{serverName}/users") });
            Console.Write("#########");
            endpoint = serviceHostUserService.AddServiceEndpoint(typeof(UserServiceDll.IUserService), new WebHttpBinding(), "api");
            endpoint.Behaviors.Add(new WebHttpBehavior());
            Console.Write("#########");
            endpoint = serviceHostUserService.AddServiceEndpoint(typeof(UserServiceDll.IRestUserService), new WebHttpBinding(), "rest");
            Console.Write("#########");
            endpoint.Behaviors.Add(new WebHttpBehavior());
            Console.Write("##########");
            try
            {
                // NServiceBus
                var endpointInstance = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();
                //SpeditionEvents.initialize(endpointInstance);

                // WCFs
                serviceHost.Open();
                Console.Write("####");
                serviceHostUserService.Open();

                //welcome screen
                Console.Write("####\n");
                Console.WriteLine("\t\t************************************************");
                Console.WriteLine("\t\t*          Server has started succesfully      *");
                Console.WriteLine("\t\t*                                              *");
                Console.WriteLine($"\t\t* Address: http://localhost/{serverName}/     *");
                Console.WriteLine($"\t\t* Address: http://localhost/{serverName}/users*");
                Console.WriteLine("\t\t************************************************");
                Console.WriteLine("\n\n\n Authors:");
                Console.WriteLine("    Daniel Serkowski 155191");
                Console.WriteLine("    Paweł Cieszko 155044");
                Console.WriteLine("\n> Press any key to stop...");

                //Process browser = new Process();
                //browser.StartInfo.FileName = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe ";
                //browser.StartInfo.Arguments = $"-incognito --new-window http://localhost/{serverName}/users/rest/login.html";
                //browser.Start();

                Console.ReadKey();
                serviceHost.Close();
                serviceHostUserService.Close();
                endpointInstance.Stop().GetAwaiter().GetResult();
            }
            catch (System.ServiceModel.AddressAccessDeniedException ex)
            {
                Console.WriteLine("####\n ERROR: Run application with administrator rights.\n\n\tDescritpion: \n" + ex.Message);
                Console.ReadKey();
            }         
        }
    }
}
