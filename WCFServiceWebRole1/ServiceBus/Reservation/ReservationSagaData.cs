﻿using NServiceBus;
using System;

namespace SpeditionService.ServiceBus.Reservation
{
    class ReservationSagaData : ContainSagaData
    {
        override public Guid Id { get; set; }

        public Guid ReservationId { get; set; }

        public Guid OrderSagaId { get; set; }

        public int? AvailableDriverId { get; set; }
    }
}
