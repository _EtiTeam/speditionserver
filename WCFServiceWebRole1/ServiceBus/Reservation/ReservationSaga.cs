﻿using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Persistence.Sql;
using ServiceBusDLL;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SpeditionService.ServiceBus.Reservation
{
    class ReservationSaga :
        SqlSaga<ReservationSagaData>,
        IAmStartedByMessages<OrderCommitted>,
        IHandleMessages<OrderReservationRollback>,
        IHandleMessages<OrderFinished>,
        IHandleTimeouts<ProviderIsReadyTimeout>
    {
        static ILog log = LogManager.GetLogger<ReservationSaga>();

        private const System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.RepeatableRead;

        protected override string CorrelationPropertyName => nameof(Data.ReservationId);

        protected override void ConfigureMapping(IMessagePropertyMapper mapper)
        {
            // it configures all messages
            mapper.ConfigureMapping<MessageBase>(message => message.Id);
        }

        private Task reserve(IMessageHandlerContext context)
        {
            var connection = new DatabaseConnection().SqlConnection;
            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                transaction = connection.BeginTransaction(isolationLevel);
                SqlCommand cmd = connection.CreateCommand();
                cmd.Transaction = transaction;

                var drivers = ModelDLL.ModelOperations.SelectUsingEstablishConnection<ModelDLL.Driver>(ModelDLL.Driver.SelectCommand, cmd);
                ModelDLL.Driver availableDriver = null;
                foreach (var driver in drivers)
                {
                    if (driver.Capacity > 0)
                    {
                        availableDriver = driver;
                        break;
                    }
                }

                if (availableDriver == null)
                {
                    log.Info($"NOT reserved. ReservationId={ Data.ReservationId }");
                    MarkAsComplete();
                    var replyMessage = new OrderProviderIsNotReady
                    {
                        Id = Data.OrderSagaId,
                        ConfirmationId = Data.ReservationId
                    };
                    return context.Send(Data.Originator, replyMessage);
                }
                else
                {
                    --availableDriver.Capacity;
                    ModelDLL.ModelOperations.UpdateUsingEstablishConnection(availableDriver, cmd);

                    log.Info($"RESERVED. ReservationId={ Data.ReservationId }");
                    Data.AvailableDriverId = availableDriver.Id;
                    return sendProviderIsReadyTimeout(context);
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                transaction = null;
                throw ex;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
                connection.Close();
            }
        }

        private Task finishReservation()
        {
            var connection = new DatabaseConnection().SqlConnection;
            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                transaction = connection.BeginTransaction(isolationLevel);
                SqlCommand cmd = connection.CreateCommand();
                cmd.Transaction = transaction;

                var availableDriver = ModelDLL.ModelOperations.SelectUsingEstablishConnection<ModelDLL.Driver>(
                    Data.AvailableDriverId.Value,
                    cmd
                );
                ++availableDriver.Capacity;
                ModelDLL.ModelOperations.UpdateUsingEstablishConnection(availableDriver, cmd);

                MarkAsComplete();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                transaction = null;
                throw ex;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
                connection.Close();
            }
        }

        private Task sendProviderIsReadyTimeout(IMessageHandlerContext context)
        {
            var replyMessage = new OrderProviderIsReady
            {
                Id = Data.OrderSagaId,
                ReservationId = Data.ReservationId
            };

            var timeout = TimeSpan.FromSeconds(7);
            return RequestTimeout(context, timeout, new ProviderIsReadyTimeout { Id = Data.Id });
        }

        private Task sendProviderIsReady(IMessageHandlerContext context)
        {
            var replyMessage = new OrderProviderIsReady
            {
                Id = Data.OrderSagaId,
                ReservationId = Data.ReservationId
            };
            return context.Send(Data.Originator, replyMessage);
        }

        public Task Handle(OrderCommitted message, IMessageHandlerContext context)
        {
            log.Info($"Fetched CommitedOrder: { message.Id } ||| Sent ProviderIsReadyTimeout");
            Data.ReservationId = message.Id; // it is set automatically 
            Data.OrderSagaId = message.OrderSagaId;

            return reserve(context);
        }

        public Task Handle(OrderReservationRollback message, IMessageHandlerContext context)
        {
            log.Info($"Fetched OrderRollback: { message.Id } ||| FROM OrderSaga: { Data.OrderSagaId }");
            return finishReservation();
        }

        public Task Handle(OrderFinished message, IMessageHandlerContext context)
        {
            log.Info($"Fetched OrderFinished: { message.Id } ||| FROM OrderSaga: { Data.OrderSagaId }");
            return finishReservation();
        }

        public Task Timeout(ProviderIsReadyTimeout state, IMessageHandlerContext context)
        {
            log.Info($"Fetched ProviderIsReadyTimeout: { state.Id } ||| FROM OrderSaga: { Data.OrderSagaId } " +
                $"||| Sent OrderFinished");

            return sendProviderIsReady(context);
        }
    }
}
