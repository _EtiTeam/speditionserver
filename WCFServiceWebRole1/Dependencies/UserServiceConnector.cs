﻿using System.ServiceModel;
using System.ServiceModel.Description;

namespace SpeditionService
{
    public static class UserServiceConnector
    {
        // interface must be dispose
        public static UserServiceDll.IUserService createInstance() {
            var f = new ChannelFactory<UserServiceDll.IUserService>(new WebHttpBinding(),
                new EndpointAddress("http://localhost/speditionService/users/api"));
            f.Endpoint.Behaviors.Add(new WebHttpBehavior());
            var c = f.CreateChannel();
            //((IDisposable)c).Dispose();
            return c;
        }
    }
}