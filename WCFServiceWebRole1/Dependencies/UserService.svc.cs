﻿using System.Data.SqlClient;

namespace SpeditionService
{
    public class UserService : UserServiceDll.UserServiceImpl
    {
        public override SqlConnection SqlConnection =>
            new DatabaseConnection().SqlConnection;

        public override string baseGuiPath => 
            GuiDLL.GuiConstants.BASE_SPEDITION_GUI_PATH;
    }
}
