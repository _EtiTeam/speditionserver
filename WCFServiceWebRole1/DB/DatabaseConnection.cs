﻿using System.Data.SqlClient;

namespace SpeditionService
{
    public class DatabaseConnection : ModelDLL.IDatabaseConnection
    {
        public SqlConnection SqlConnection => getConnection();

        private static SqlConnection getConnection()
        {
            SqlConnection c = new SqlConnection();
            c.ConnectionString = Properties.Settings.Default.ConnectionString; // from Properties/Settings.settings
            return c;
        }
    }
}