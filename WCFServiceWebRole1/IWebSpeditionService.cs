﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SpeditionService
{
    [ServiceContract]
    public interface ISpeditionService
    {
        [OperationContract]
        [WebGet(UriTemplate = "")]
        void redirection();
    }
}
